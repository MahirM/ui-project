package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.control.TextField;

import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Mahir on 14.09.2017..
 */
public class Customer extends Pane {

    private TextField customer;
    private TextField qnt;
    private TextField price;
    private ChoiceBox<String> type;
    private DatePicker date;
    private Button submit;
    private ArrayList<CustomerInfo> customerInfos = new ArrayList<>();


    public Customer() {
        setPrefSize(400, 400);

        customer = new TextField("Unesite ime kupca");
        customer.setLayoutY(20);
        customer.setLayoutX(20);
        customer.setPrefWidth(220);
        customer.setOnMouseClicked(event -> customer.clear());
        getChildren().add(customer);
        customer.toString();

        qnt = new TextField("Unesite cijenu");
        qnt.setLayoutY(70);
        qnt.setLayoutX(20);
        qnt.setPrefWidth(220);
        qnt.setOnMouseClicked(event -> qnt.clear());
        getChildren().add(qnt);
        qnt.toString();

        price = new TextField("Unesite količinu");
        price.setLayoutY(120);
        price.setLayoutX(20);
        price.setPrefWidth(220);
        price.setOnMouseClicked(event -> price.clear());
        getChildren().add(price);
        price.toString();

        type = new ChoiceBox<String>();
        type.setStyle("-fx-background-color: #FF7619;" +
                " -fx-border-radius: 10px;" +
                "-fx-background-radius: 5px;");
        type.setLayoutY(170);
        type.setLayoutX(20);
        type.setPrefWidth(220);
        type.getItems().addAll("Digitalna štampa-color", "Digitalna štampa-crno bijelo",
                "Digitalna štampa-obostrano", "Digitalna štampa-jednostrano", "Štampa velikog formata");
        type.setValue("Digitalna štampa-crno bijelo");
        getChildren().add(type);
        type.toString();

        date = new DatePicker();
        date.setLayoutY(220);
        date.setLayoutX(20);
        date.setPrefWidth(220);
        date.setValue(LocalDate.now());
        getChildren().add(date);

        submit = new Button("Spremi");
        submit.setStyle("-fx-background-color: #FF7619;" +
                " -fx-border-radius: 10px;" +
                "-fx-background-radius: 10px;");
        submit.setLayoutY(270);
        submit.setLayoutX(187);
        getChildren().add(submit);
        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CustomerInfo info =
                        new CustomerInfo(customer.getText(), price.getText(), qnt.getText(), type.getValue(), date.getValue().toString());
                StringBuilder stringBuffer = new StringBuilder();
                stringBuffer
                        .append(info.getName())
                        .append(",")
                        .append(info.getPrice())
                        .append(",")
                        .append(info.getQnt())
                        .append(",")
                        .append(info.getType())
                        .append(",")
                        .append(info.getDate())
                        .append("\n");
                try {
                    if (!new File("test.txt").exists()) {
                        new File("test.txt").createNewFile();
                    }
                    Files.write(Paths.get("test.txt"), stringBuffer.toString().getBytes(), StandardOpenOption.APPEND);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

//    private boolean isInt(TextField input, String message) {
//        try {
//            int age = Integer.parseInt(input.getText());
//            return true;
//        } catch (NumberFormatException e) {
//            return false;
//        }
//
//
//    }

    public static void saveAllCustomersInFile(ArrayList<CustomerInfo> customerInfos) {
        if (customerInfos.size() == 0) {
            return;
        }
        StringBuilder allData = new StringBuilder();
        for (CustomerInfo customerInfo : customerInfos) {
            allData
                    .append(customerInfo.getName())
                    .append(",")
                    .append(customerInfo.getPrice())
                    .append(",")
                    .append(customerInfo.getQnt())
                    .append(",")
                    .append(customerInfo.getType())
                    .append(",")
                    .append(customerInfo.getDate())
                    .append("\n");
        }

        System.out.println(allData);
        try {
            Files.write(Paths.get("test.txt"), allData.toString().getBytes(), StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<CustomerInfo> readAllCustomersFromFile() {
        ArrayList<CustomerInfo> customerInfos = new ArrayList<>();
        try {
            byte[] encoded = Files.readAllBytes(Paths.get("test.txt"));
            String result = new String(encoded, "UTF-8");
            String[] data = result.split("\n");

            for (int i = 0; i < data.length; i++) {
                String[] customer = data[i].split(",");
                if ((customer.length == 5)) {
                    customerInfos.add(new CustomerInfo(customer[0], customer[1], customer[2], customer[3], customer[4]));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return customerInfos;
    }

    public void deleteAllCustomersFromFile() {
        if (customerInfos.size() == 0) {
            return;
        }
        StringBuilder allData = new StringBuilder();
        for (CustomerInfo customerInfo : customerInfos) {
            allData
                    .append(customerInfo.getName())
                    .append(",")
                    .append(customerInfo.getPrice())
                    .append(",")
                    .append(customerInfo.getQnt())
                    .append(",")
                    .append(customerInfo.getType())
                    .append(",")
                    .append(customerInfo.getDate())
                    .append("\n");
        }
        System.out.println(allData);
        try {
            Files.write(Paths.get("test.txt"), allData.toString().getBytes(), StandardOpenOption.CREATE
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
