package sample;

/**
 * Created by Mahir on 16.09.2017..
 */
public class CustomerInfo {
    private String name;
    private String price;
    private String qnt;
    private String type;
    private String date;

    public CustomerInfo(String name, String price, String qnt, String type, String date) {
        this.name = name;
        this.price = price;
        this.qnt = qnt;
        this.type = type;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQnt() {
        return qnt;
    }

    public void setQnt(String qnt) {
        this.qnt = qnt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
