package preview;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import sample.Customer;
import sample.CustomerInfo;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    public ListView listView;
    public TextField customer;
    public TextField price;
    public TextField qnt;
    public ChoiceBox<String> type;
    public DatePicker date;
    public Button edit;
    public Button save;
    public Button delete;
    private ArrayList<CustomerInfo> customerInfos;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        customer.setEditable(false);
        price.setEditable(false);
        qnt.setEditable(false);
        type.setDisable(true);
        date.setEditable(false);

        type.getItems().addAll("Digitalna štampa-color", "Digitalna štampa-crno bijelo",
                "Digitalna štampa-obostrano", "Digitalna štampa-jednostrano", "Štampa velikog formata");
        type.setStyle("-fx-background-color: #CF5300;" +
                " -fx-border-radius: 10px;" +
                "-fx-background-radius: 5px;");
        customerInfos = Customer.readAllCustomersFromFile();
        listView.setItems(FXCollections.observableList(customerInfos));
        listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<CustomerInfo>() {
            @Override
            public void changed(ObservableValue<? extends CustomerInfo> observable, CustomerInfo oldValue, CustomerInfo newValue) {
                customer.setEditable(false);
                price.setEditable(false);
                qnt.setEditable(false);
                type.setDisable(true);
                date.setEditable(false);

                customer.setText(newValue.getName());
                price.setText(newValue.getPrice());
                qnt.setText(newValue.getQnt());
                type.setValue(newValue.getType());
                date.setValue(LocalDate.parse(newValue.getDate()));
            }
        });
        edit.setStyle("-fx-background-color: #FF7619;" +
                " -fx-border-radius: 10px;" +
                "-fx-background-radius: 10px;");
        edit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (listView.getSelectionModel().getSelectedIndex() == -1){
                    return;
                }
                customer.setEditable(true);
                price.setEditable(true);
                qnt.setEditable(true);
                date.setEditable(true);
                type.setDisable(false);
            }
        });
        save.setStyle("-fx-background-color: #FF7619;" +
                " -fx-border-radius: 10px;" +
                "-fx-background-radius: 10px;");
        save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (listView.getSelectionModel().getSelectedIndex() == -1){
                    return;
                }

                CustomerInfo current =((CustomerInfo)listView.getSelectionModel().getSelectedItem());
                current.setName(customer.getText());
                current.setPrice(price.getText());
                current.setQnt(qnt.getText());
                current.setType(type.getValue());
                current.setDate(String.valueOf(date.getValue()));
                Customer.saveAllCustomersInFile(customerInfos);
            }
        });
        delete.setStyle("-fx-background-color: #FF7619;" +
                " -fx-border-radius: 10px;" +
                "-fx-background-radius: 10px;");
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (listView.getSelectionModel().getSelectedIndex() == -1){
                    return;
                }

                CustomerInfo current =((CustomerInfo)listView.getSelectionModel().getSelectedItem());
                int selectedId = listView.getSelectionModel().getSelectedIndex();
                listView.getItems().remove(selectedId);
                customerInfos.remove(current);
                Customer.saveAllCustomersInFile(customerInfos);

            }
        });
    }
}
