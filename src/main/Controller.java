package main;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.Customer;
import sample.CustomerInfo;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public Button submit;
    public Button preview;

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        Tooltip tip = new Tooltip();
        Image image = new Image ("tooltip1.jpg");
        tip.setGraphic(new ImageView(image));
        submit.setTooltip(tip);
        submit.setStyle("-fx-background-color: #FF7619;" +
                " -fx-border-radius: 10px;" +
                "-fx-background-radius: 10px;");
        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            new sample.Main().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        Tooltip tip2 = new Tooltip();
        Image image2 = new Image ("tootip2.jpg");
        tip2.setGraphic(new ImageView(image2));
        preview.setStyle("-fx-background-color: #FF7619;" +
                " -fx-border-radius: 10px;" +
                "-fx-background-radius: 10px;");
        preview.setTooltip(tip2);
        preview.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            new preview.Main().start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
